#include "test_collision.h"

//Fonction qui verifie si il y a une collision
int collision(POINT pos_future, POINT limite, char map[][MAX_X])
{

    //Verifie si la position future se trouve en dehors de la map
    if ( (pos_future.x >= limite.x) || (pos_future.y >= limite.y) || (pos_future.x < 0) || (pos_future.y < 0))
    {
        //Deplacement impossible
        return test_collision = COLLISION;
    }
    //Verifie si il y a un obstacle a la positon future
    else if( (map[pos_future.y][pos_future.x] != ' ') && !(49 <= map[pos_future.y][pos_future.x] && map[pos_future.y][pos_future.x] <= 53) )
    {
        //Deplacement impossible
        return test_collision = COLLISION;
    }
    //Retourne NO_COLLISION si le deplacement est possible
    else
    {
        //Deplacement possible
        return test_collision = NO_COLLISION;
    }
}


//Fonction qui effectue l'eventuel deplacement
void deplacement(POINT *joueur,POINT deplacement, int test)
{
        // Verifie que ce n est pas la valeur de collision
        if(test == 0)
        {
             (*joueur).x = deplacement.x;
             (*joueur).y = deplacement.y;
        }
}
