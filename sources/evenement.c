#include "evenement.h"

void evenement_clavier(POINT* joueur, char  map[][MAX_X], int* quitter)
{
  
  // point intermediaire pour les deplacements
  struct point position = {0,0};
  //point maximum de la map
  struct point limite = {MAX_X,MAX_Y};

  //valeur intermediaire pour les collisions
  int test;
  
  //recupere le premiere touche appuyer
  char r;
  r = getch();

  //initialise les valeurs du point intermediaire
  position.x = (*joueur).x;
  position.y = (*joueur).y;


  //analyse chaque cas de touche appuyer qui a une action a effectuer
  switch(r)

  {
      /*  TOUCHES DES DEPLACEMENTS */
      //haut
      case 'z':
          {
          //position future
          position.y -= 1;
          //test si la position future est possible et les collisions engendrees
          test = collision(position, limite, map);
          //deplace le joueur a la position future suivant les collisions
          deplacement(joueur, position, test);
          }
      break;
      //gauche
      case 'q':
          {
          position.x -= 1;
          test = collision(position, limite, map);
          deplacement(joueur, position, test);
          }
      break;
      //bas
      case 's':
          {
          position.y += 1;
          test = collision(position, limite, map);
          deplacement(joueur, position, test);
          }
      break;
      //droite
      case 'd':
          {
          position.x += 1;
          test = collision(position, limite, map);
          deplacement(joueur, position, test);
          }
      break;
      
      /* TOUCHE POUR QUITTER */
      case 'p':
          //arrete la boucle de rafraichissement
          *quitter = 1;
      break;
      
      /* DEFAULT */
      default:
      break;
  }
}
