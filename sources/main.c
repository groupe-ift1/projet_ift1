/*Fonction getch récupérée sur un programme de Eric Bachard :) */

#include<stdlib.h>
#include<stdio.h>

#include <termios.h>
#include <linux/fd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "efface_ecran.h"
#include "generation_de_map.h"
#include "point.h"
#include "evenement.h"
#include "test_collision.h"
#include "getch.h"

/*
struct objet
{
    POINT pos;
    char forme;
};
*/

/*
void manipulation_map(int decalage, char map[MAX_Y][MAX_X])
{
	int i;
	int n;
        for(i = 0; i < 6; i++)
        {
            for(n = decalage; n < decalage+15 ; n++ )
            {
                fprintf(stdout, " %c", map[i][n]);
            }
            fprintf(stdout,"\n");

        }
}
*/

int main(void)
{
    char map[MAX_Y][MAX_X];
    
    //joueur local
    POINT joueur = {4, 4};

    //tableau des autres joueurs
    POINT tab_joueur[4];

    //variable boucle
    int i;
    int n;

    // initialisation tab_joueur (simulation)
    //=======================================
    
    for(i = 0; i < 4; i++)
    {
      tab_joueur[i].x = -1;
    }
		
    tab_joueur[1].y = 0;
    tab_joueur[1].x = 3;

    tab_joueur[0].y = 0;
    tab_joueur[0].x = 3;
		
		
	///////////////////////////////////////////
    // BOUCLE DE RAFRAICHISSEMENT PRINCIPALE //
    ///////////////////////////////////////////
    
    int quitter = 0;
    
    do
    {

        //fonction evenement
        evenement_clavier(&joueur, map, &quitter);

        //fonction lecture fichier pour la map
        ouverture_fichier(map);

        //fonction de recuperation et d'envoie des positions
        //multijoueur(POINT tab_joueur[4]);
    
        //simule le deplacement d'un joueur
        tab_joueur[1].x++;

        //affiche les joueurs simules
        for(i = 0; i < 4; i++)
        {
          if(tab_joueur[i].x != -1)
             map[tab_joueur[i].y][tab_joueur[i].x] = i+50;
        }

        //rajoute le joueur local dans la map
        map[joueur.y][joueur.x] = '1';

        efface_ecran();

        //affiche la map
        for(i = 0; i<MAX_Y; i++)
        {
            for(n = 0; n<MAX_X; n++)
            {
                fprintf(stdout, " %c", map[i][n]);
            }
            fprintf(stdout,"\n");
        }
    }
    while(quitter != 1);

#ifndef DEBUG
    efface_ecran();
#endif

    return 0;
}


