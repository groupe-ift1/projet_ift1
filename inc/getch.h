/*Fonction getch récupérée sur un programme de Eric Bachard */

#ifndef _GETCH_H
#define _GETCH_H

#include<stdlib.h>
#include<stdio.h>

#include <termios.h>
#include <linux/fd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

char getch(void);

#endif /* _GETCH_H */