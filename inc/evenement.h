#ifndef _EVENEMENT_H
#define _EVENEMENT_H

#include "point.h"
#include "getch.h"
#include "test_collision.h"

//fonction qui gere le clavier et les actions liees aux touches
void evenement_clavier(POINT* joueur, char  map[][MAX_X], int* quitter);

#endif /* _EVENEMENT_H */