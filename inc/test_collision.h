#ifndef _TEST_COLLISION
#define _TEST_COLLISION

#include "point.h"
#include "generation_de_map.h"

//enum des possibilites de collision
enum verif_collision
{
    NO_COLLISION = 0,
    COLLISION = 1,

}test_collision;

//fonction qui verifie si il y a une collision au point donne (ou s'il se trouve en dehors de la map) et retourne une valeur de enum verif_collision
int collision(POINT pos_future, POINT limite, char map[][MAX_X]);

//fonction qui effectue un deplacement suivant la valeur test qui est un enum verif_collision
void deplacement(POINT *joueur,POINT deplacement, int test);

#endif /*_TEST_COLLISION*/