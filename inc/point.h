#ifndef _POINT_H
#define _POINT_H


//structure pour controler des points dans le double tableau map[][]
struct point
{
    int x;
    int y;
};


typedef struct point POINT;

#endif /*_POINT_H*/